from flask import Flask, render_template, request
import random

app = Flask(__name__)

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/guess", methods=["POST"])
def guess():
    number = random.randint(0, 10)
    guess = int(request.form["guess"])
    counter = int(request.form["counter"])

    counter += 1

    if guess == number:
        message = f"You guessed it! My number was {number} and it took you {counter} guesses."
    elif guess < number:
        message = "Your guess is too low."
    else:
        message = "Your guess is too high."

    return render_template("result.html", message=message, counter=counter)

if __name__ == "__main__":
    app.run(debug=True)
