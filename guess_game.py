import random

# store a random numbert between 0 to 100
num = random.randint(0, 100)

# print welcome message to the guessing game
print("Welcome to the guessing game!")

# create a loop that will continue until the user guesses the number
while True:
	# ask the user to guess a number
	guess = input("Guess a number between 0 and 100: ")

	# convert the user's guess to an integer
	guess = int(guess)
    
	# check if the user's guess is correct
	if guess == num:
		# if the user's guess is correct, print the message
		print("You guessed the number!")
	elif guess < num:
		# if the user's guess is less than the number, print the message
		print("Your guess is too low.")
	elif guess > num:
		# if the user's guess is greater than the number, print the message
		print("Your guess is too high.")
	else:
		# if the user's guess is not correct, print the message
		print("Invalid input.")