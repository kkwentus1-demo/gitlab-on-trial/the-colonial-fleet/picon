import random

# Store a random number between 0 to 10 
number = random.randint(0, 10)
counter = 0
# Print a welcome message to Yael, ask to guess a number between 0 to 10

print("Welcome to the guessing game, Yael!")
print("I am thinking of a number between 0 and 10, can you guess what it is?")

# Create a loop that will continue until the user guesses the number

while True:
    # Get the user's guess
    guess = int(input("Guess my number: "))
    counter += 1


   # Check if the user's guess is correct
    if guess == number:
        # If the guess is correct, printa message and break out of the loop
        print("You guessed it! My number was", number, "and it took you", counter, "guesses.")
        break
    
    elif    guess < number:
        print("Your guess is too low.")
    else:
        print("Your guess is too high.")